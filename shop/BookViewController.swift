import UIKit

let lip = [
    "Lorem ipsum dolor sit amet neque. Integer hendrerit purus at eros quis enim. Suspendisse adipiscing. Nam ullamcorper et, ultricies lobortis eu."
    ,
    "Etiam urna. Phasellus sagittis eget, molestie mauris. Praesent nec massa. Integer quis orci. Suspendisse potenti. Quisque sit amet sagittis lorem."
    ,
    "Praesent tortor pede sit amet ante. Vivamus est eu libero. Vivamus metus wisi, id nisl. Nunc justo. Proin dapibus, felis in risus. Ut facilisis. Phasellus consequat. Quisque consectetuer pede, at lectus semper eros. Donec ullamcorper et, placerat id, adipiscing at, congue dolor. Suspendisse vel quam volutpat eu."
    ,
    "Nulla vehicula ut, gravida massa ac nunc. Nam consectetuer lobortis ut, dolor. Maecenas bibendum tellus, imperdiet faucibus orci luctus mauris ac turpis viverra."
    ,
    "Pellentesque habitant morbi tristique eget, bibendum metus tellus, quis molestie a, dui. Integer pharetra pede. Integer nonummy condimentum magna auctor tortor turpis, accumsan fringilla orci. Proin cursus justo imperdiet consequat. Cum sociis natoque penatibus et ultrices posuere eu."
    ,
    "Donec consectetuer vulputate aliquam arcu. In quis ante. Duis quam volutpat ut, lobortis elit. Nam aliquet at, suscipit ultricies, velit wisi, vitae quam. Integer ut justo."
    ,
    "Nunc placerat consequat. Quisque condimentum. Donec viverra elit. Curabitur interdum. Integer adipiscing. Vestibulum at bibendum eget, tortor. Integer eu nisl urna fringilla orci. Nullam euismod, quam felis tincidunt rutrum et, varius nunc, lobortis laoreet."
    ,
    "Pellentesque et velit vel neque eu pulvinar at, ornare laoreet. Nam eu risus. Sed in dui. Vivamus a diam. Curabitur quam ipsum, vel turpis risus ante eu pede ultrices vel, augue."
    ,
    "Suspendisse nulla dictum commodo, volutpat eu, tristique lorem. Vestibulum ornare at, egestas sit amet, consectetuer nisl. Sed eu libero ac hendrerit ultricies. Donec vel nulla. Proin gravida elit. Pellentesque nonummy at, convallis ligula non leo lacus, ullamcorper nec, molestie quis, ultricies."
    ,
    "Suspendisse gravida. Duis mauris quis lacus magna, porttitor egestas. Integer tristique tempus urna mattis metus ac quam auctor mattis. Pellentesque tincidunt luctus. Phasellus vitae velit sit amet ipsum feugiat facilisis nibh, mollis risus. Suspendisse fermentum facilisis. Nulla posuere cubilia Curae, Nulla lobortis augue."
    ,
    "Praesent quis diam. Suspendisse rhoncus non, vehicula magna sapien, lacinia eget, purus. Aenean mollis neque sollicitudin lorem, pretium erat vitae ante. Vestibulum ante at tortor. Morbi sodales quam. Nunc viverra, enim pharetra volutpat. Vivamus iaculis, purus."
    ,
    "Vivamus est. Sed nibh. Donec enim fringilla fringilla elementum quis, ipsum. Duis dictum. Curabitur urna ut diam eu ipsum. Aliquam."
];


class BookViewController: UIViewController {

    //@IBOutlet weak var poster: UIImageView!
    
    @IBOutlet weak var bookViewoster: UIImageView!
    @IBOutlet weak var bookViewTitle: UILabel!
    @IBOutlet weak var bookViewDesc: UILabel!
    @IBOutlet weak var bookViewPrice: UILabel!
    
    @IBOutlet weak var cartButton: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cat = products[categoryIndex] as! [[Any]];
        let titlesbv =  cat[TITLE_INDEX] as! [String];
        let pricesbv =  cat[PRICE_INDEX] as! [Double];
        let imgbv =  cat[IMAGE_INDEX] as! [String];
        
        bookViewTitle.text = titlesbv[itemIndex];
        
        bookViewoster.image = UIImage(named:imgbv[itemIndex])

        let randomNum:UInt32 = arc4random_uniform(UInt32(lip.count))
        bookViewDesc.text = lip[Int(randomNum)]
        
        bookViewPrice.text = String(pricesbv[itemIndex])
        self.navigationItem.rightBarButtonItem = self.cartButton
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func openCart(_ sender: Any) {
        performSegue(withIdentifier: "BookToCart", sender: self)
    }
    
    @IBAction func butItem(_ sender: Any) {
        cart.append(itemIndex)
        catCart.append(categoryIndex)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}
