import UIKit

var categories = ["Informatyka", "Dla dzieci", "Historia", "Komiksy", "Biznes i rozwój", "Plyty"]
var itTitles = ["Czysty kod", "Programowanie dla początkujących", "Język C++. Szkoła programowania", "Java. Rusz głową!", "Python. Rusz głową!", "Algorytmy. Ilustrowany przewodnik", "Wordpress dla początkujących", "Jak działa Linux", "Adaptywny kod. Zwinne programowanie", "C#. Rusz głową"]
var itPrices = [56.99, 17.49, 81.99, 86.99, 88.99, 48.99, 38.99, 61.99, 61.99, 88.99, 62.49]
var itImages = ["czystykod", "programowaniedlapoczatkujacych", "jezykcpp", "headfirstjava", "headfirstpython", "algorytmy", "wordpress", "linux", "adaptywnykod", "headfirstcsharp"]
var forKidsTitles = ["Nela i polarne zwierzęta", "Absolutnie fantastyczne labirynty", "Dziennik cwaniaczka", "Naciśnij mnie", "Magiczne drzewo", "Brulion zabaw na święta"]
var forKidsPrices = [39.90, 17.94, 23.99, 23.92, 32.99, 14.99]
var forKidsPhotos = ["nela", "labirynty", "cwaniaczek", "nacisnij", "magicznedrzewo", "brulionzabaw"]
var historyTitles = ["Tajemnicze historie II wojny światowej", "Na krańce świata", "Templariusze", "Niemcy. Opowieści niepoprawne politycznie", "Złowrogi cień Marszałka", "Polskie imperium"]
var historyPrices = [32.99, 55.93, 36.99, 36.99, 49.49, 32.99]
var historyPhotos = ["tajemnicewojny", "nakrance", "templariusze", "niemcy", "marszalek", "polskieimperium"]
var comicTitles = ["Rycerze. Dziennik bohatera", "Kajko i Kokosz. Wielki turniej", "Opowieści makabryczne", "Asteriks w Italii", "Wilkołak", "Paper Girls", "Star Wars Legendy"]
var comicPrices = [26.24, 16.49, 33.99, 17.99, 26.24, 32.00, 44.99]
var comicPhotos = ["rycerze", "kajkoikokosz", "makabryczne", "asteriks", "wilkolak", "powergirld", "starwars"]
var businessTitles = ["The art of the deal", "Nigdy się nie poddawaj!", "Sztuka wojny", "Giełda. Podstawy inwestowania", "Marketing 4.0", "Inteligentny inwestor", "Psychologia sprzedaży", "Kapitał w XXI wieku", "Projektowanie doświadczeń", "Liderzy jedzą na końcu", "Wojna o pieniądz"]
var businessPrices = [46.99, 28.49, 24.49, 32.99, 40.11, 56.99, 40.99, 69.99, 62.99, 32.99, 36.99]
var businessPhotos = ["trump", "niepoddawaj", "sztukawojny", "gielda", "marketing", "inteligentnyinwestor", "psychologiasprzedazy", "kapital", "projektowaniedoswiadczen", "liderzyjedza", "wojnaopieniadz"]
var musikTitles = ["Golec UOrkiestra 1", "Golec UOrkiestra 2", "Golec UOrkiestra 3", "Golec UOrkiestra 4", "Golec UOrkiestra 5", "The best of Golec UOrkiestra", "Lindsey Stirling", "Shatter Me","Brave Enough", "Warmer in Winter", "Lust Life", "Ultraviolence", "Ghost Stories", "A Head full of Dreams", "X", "1989", "Reputation", "Red", "Speak now", "The Very Best of Chopin"]
var musikPrices = [34.49, 34.49, 34.49, 34.49, 34.49, 46.99, 41.49, 41.99, 40.99, 43.99, 41.49, 41.99, 29.99, 29.99, 29.99, 37.99, 57.99, 37.99, 37.99, 27.99]
var musikPhotos = ["golec1", "golec2", "golec3", "golec4", "golec5", "bestofgolec", "lindsey1", "lindsey2", "lindsey3", "lindsey4", "lustlife", "ultraviolence", "ghoststories", "headfull", "x", "1989", "reputation", "red", "speaknow", "chopin" ]

var products = [0 : [itTitles, itPrices, itImages], 1 : [forKidsTitles, forKidsPrices, forKidsPhotos], 2 : [historyTitles, historyPrices, historyPhotos], 3 : [comicTitles, comicPrices, comicPhotos], 4 : [businessTitles, businessPrices, businessPhotos], 5 : [musikTitles, musikPrices, musikPhotos]]

let TITLE_INDEX = 0
let PRICE_INDEX = 1
let IMAGE_INDEX = 2

var categoryIndex = -1;
var itemIndex = -1;

//var kurwa = products[categoryIndex][TITLE_INDEX][itemIndex]

class CategoryTableViewController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath:
        IndexPath) -> UITableViewCell {
        let cellIdentifier = "CategoryCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! CategoryTableViewCell
        cell.nameLabel?.text = categories[indexPath.row]
        if(indexPath.row == categories.count - 1) {
            cell.nameLabel?.backgroundColor = UIColor(red: 39.0 / 255.0, green: 174.0 / 255.0, blue: 96.0 / 255.0, alpha: 0.5)
        }
        
        var cat = products[indexPath.row] as! [[Any]]
        let postersbv = cat[IMAGE_INDEX] as! [String]
        
        cell.background?.image = UIImage(named: postersbv[0])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        categoryIndex = indexPath.row;
        performSegue(withIdentifier: "ItemListViewController", sender: self)
    }
    
    @IBAction func unwindToVC1(segue:UIStoryboardSegue) { }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
