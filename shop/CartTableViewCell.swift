import UIKit

class CartTableViewCell: UITableViewCell {

    @IBOutlet weak var cartTitleLabel: UILabel!
    
    @IBOutlet weak var cartCategoryLabel: UILabel!
    
    @IBOutlet weak var cartPriceLabel: UILabel!
    
    @IBOutlet weak var cartPoster: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
