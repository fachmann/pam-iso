// https://medium.com/yay-its-erica/creating-a-music-player-app-in-swift-3-53809471f663
// https://developer.apple.com/library/content/qa/qa1913/_index.html

import UIKit

import AVFoundation

class MusicViewController: UIViewController {
    
    var songPlayer: AVAudioPlayer = AVAudioPlayer()
    var hasBeenPaused = false

    @IBOutlet weak var musicTitle: UILabel!
    @IBOutlet weak var musicPoster: UIImageView!
    @IBOutlet weak var musicPrice: UILabel!
    @IBOutlet weak var cartButton: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareSongAndSession()

        self.navigationItem.rightBarButtonItem = self.cartButton
        
        let cat = products[categoryIndex] as! [[Any]];
        let titlesbv =  cat[TITLE_INDEX] as! [String];
        let pricesbv =  cat[PRICE_INDEX] as! [Double];
        let imgbv =  cat[IMAGE_INDEX] as! [String];
        
        musicTitle.text = titlesbv[itemIndex];
        musicPoster.image = UIImage(named:imgbv[itemIndex])
        musicPrice.text = String(pricesbv[itemIndex])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func openCart(_ sender: Any) {
        performSegue(withIdentifier: "MusicToCart", sender: self)
    }
    
    @IBAction func play(_ sender: Any) {
        songPlayer.play()
    }
    
    @IBAction func buyItem(_ sender: Any) {
        cart.append(itemIndex)
        catCart.append(categoryIndex)
    }
    
    @IBAction func pause(_ sender: Any) {
        if songPlayer.isPlaying {
            songPlayer.pause()
            hasBeenPaused = true
        } else {
            hasBeenPaused = false
        }
    }
    @IBAction func rewind(_ sender: Any) {
        if songPlayer.isPlaying || hasBeenPaused {
            songPlayer.stop()
            songPlayer.currentTime = 0
            
            songPlayer.play()
        } else  {
            songPlayer.play()
        }
    }
    func prepareSongAndSession() {
        if let asset = NSDataAsset(name:"golec"){
            
            do {
                // Use NSDataAsset's data property to access the audio file stored in Sound.
                songPlayer = try AVAudioPlayer(data:asset.data, fileTypeHint:"wav")
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
